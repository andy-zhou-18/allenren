var position = 0;
var playList;
var video;

window.onload = function () {
    playList = [
        'videos/fuyun.mp4',
        'videos/daoxiang.mp4'
    ];
    video = document.getElementById('video');
    video.addEventListener('ended', nextVideo, false);
    video.src = playList[position];
    video.load();
    video.play();
}

function nextVideo() {
    position++;
    if (position >= playList.length) {
        position = 0;
    }
    video.src = playList[position];
    video.load();
    video.play();
}